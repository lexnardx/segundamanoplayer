//
//  FavoritesPresenter.swift
//  Player
//
//  Created by leonardo Orihuela on 8/12/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

protocol FavoritesPresenterDelegate : AnyObject{
    func didFetchResults(results : Array<OArtist>)
    func favoriteFlagChanged(idArtist : String)
}

class FavoritesPresenter: NSObject {

    weak var delegate : FavoritesPresenterDelegate?

    func removeFavorite(artist : OArtist) {

        if artist.trackId == nil {return}
        let defaults = UserDefaults.standard

        var favoritesDict = NSMutableDictionary()
        let favorites = favoritesDictionary()
        if  favorites.allValues.count > 0 {
            favoritesDict = NSMutableDictionary(dictionary: favorites)
        }

        favoritesDict.setValue(NSDictionary(), forKey: artist.trackId!)
        defaults.set(favoritesDict, forKey: "favorites")
        defaults.synchronize()

        fetchFavorites()
    }

    func fetchFavorites() {
        let favorites = favoritesDictionary()
        var favoritesArray = Array<OArtist>()

        for item in favorites.allValues {
            let artist = OArtist(fromJson: item as! NSDictionary)
            if artist.trackId != nil {
                favoritesArray.append(artist)
            }
        }

        self.delegate?.didFetchResults(results: favoritesArray)
    }


    func favoritesDictionary() -> NSMutableDictionary {

        var favorites = NSMutableDictionary()
        let defaults = UserDefaults.standard
        if let favoritesDefaults : AnyObject = defaults.object(forKey: "favorites") as AnyObject? {
            favorites = favoritesDefaults as! NSMutableDictionary
        }

        return favorites
    }

}
