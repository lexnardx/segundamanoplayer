//
//  ArtistTableViewCell.swift
//  Player
//
//  Created by leonardo Orihuela on 8/11/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit
import SDWebImage

class ArtistTableViewCell: UITableViewCell {

    var artistImg : UIImageView?
    var favoriteImg : UIImageView?
    var artistLbl : UILabel?
    var songLbl : UILabel?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    func setupCellWithSize(size : CGSize){

        if(artistLbl != nil){ return}

        artistImg = UIImageView(frame: CGRect(x: size.width * 0.05, y: 0, width: size.height * 0.68, height: size.height * 0.68))
        artistImg?.center = CGPoint(x: artistImg!.center.x, y: size.height/2)
        artistImg?.layer.shadowOffset = CGSize(width: 1, height: 1)
        artistImg?.layer.shadowColor = UIColor.black.cgColor
        artistImg?.layer.shadowRadius = 1
        self.contentView.addSubview(artistImg!)

        songLbl = UILabel(frame: CGRect(x: size.width * 0.2, y: 0, width: size.width * 0.75, height: size.height/3))
        songLbl?.titleAppearance()
        songLbl?.center = CGPoint(x: songLbl!.center.x, y: size.height * 0.4)
        self.contentView.addSubview(songLbl!)

        artistLbl = UILabel(frame: CGRect(x: size.width * 0.2, y: 0, width: size.width * 0.75, height: size.height/3))
        artistLbl?.subtitleAppearance()
        artistLbl?.center = CGPoint(x: artistLbl!.center.x, y: size.height * 0.75)
        self.contentView.addSubview(artistLbl!)

        favoriteImg = UIImageView(frame: CGRect(x: size.width * 0.85, y: 0, width: size.height * 0.3, height: size.height * 0.3))
        favoriteImg?.center = CGPoint(x: favoriteImg!.center.x, y: size.height/2)
        self.contentView.addSubview(favoriteImg!)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func loadImageUrl(imageUrl:String) {
        artistImg?.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage())
    }
    
}
