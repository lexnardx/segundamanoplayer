//
//  SearchViewController.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,SearchPresenterDelegate,UISearchResultsUpdating,UITableViewDelegate,UITableViewDataSource {



    private var searchController = UISearchController(searchResultsController: nil)
    private var searchPresenter = SearchPresenter()
    private var tableView : UITableView?
    private var dataSource : Array<OArtist>?
    private let cellHeight = UIUtils.heightProportion(proportion: 8)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }

    private func setupView() {

        self.title = "Favorites"

        dataSource =  Array<OArtist>()

        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: UIUtils.widthProportion(proportion: 100), height: UIUtils.heightProportion(proportion: 100)))
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.register(ArtistTableViewCell.self, forCellReuseIdentifier: "reuseArtist")
        tableView?.rowHeight = cellHeight
        self.view.addSubview(tableView!)

        searchPresenter.delegate = self

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search content"
        definesPresentationContext = true

        //navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false


    }

    //MARK: - UISearchController

    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text!.count > 0 {
            DispatchQueue.main.async {[weak self] in
                self?.searchPresenter.searchArtist(artist:searchController.searchBar.text!)
            }
        }
    }

    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    //MARK: - UITableView

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("datasource count \(String(describing: dataSource))")
        return dataSource!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let artist = dataSource![indexPath.row]
        let artistCell = tableView.dequeueReusableCell(withIdentifier: "reuseArtist") as! ArtistTableViewCell

        print(artist.identifier!)

        artistCell.setupCellWithSize(size: CGSize(width: tableView.frame.size.width, height: cellHeight))
        artistCell.artistLbl!.text = artist.name
        artistCell.songLbl!.text = artist.trackName
        artistCell.loadImageUrl(imageUrl: artist.artworkUrl!)

        return artistCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let artist = dataSource![indexPath.row]
        PlayerView.sharedPlayer.playContent(content: artist)
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { (action, indexPath) in
            self.searchPresenter.saveFavorite(artist: self.dataSource![indexPath.row])
        }
        favorite.backgroundColor = .mainColor

        return [favorite]
    }

    //MARK: - Presenter Delegates

    func didFetchResults(results: Array<OArtist>) {
        DispatchQueue.main.async {[weak self] in
            self?.dataSource?.removeAll()
            if results.count > 0 {
                self?.dataSource?.append(contentsOf: results)
            }
            self?.tableView?.reloadData()
        }
    }


    func favoriteFlagChanged(idArtist: String) {

    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
