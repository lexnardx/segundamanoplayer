//
//  SearchPresenter.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

protocol SearchPresenterDelegate : AnyObject{
    func didFetchResults(results : Array<OArtist>)
    func favoriteFlagChanged(idArtist : String)
}


class SearchPresenter: NSObject {

    weak var delegate : SearchPresenterDelegate?
    private var connectionManager = ConnectionManager()

    func searchArtist(artist : String) {

        print("searching \(artist)")

        let url = String(format:"%@&term=%@",connectionManager.baseURLArtist,artist.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

        connectionManager.startGET(url: url) { (result : Any?, error : Error?) in
            if let results = result {
                self.delegate?.didFetchResults(results: JSONHelper.parseArtists(dictionary: results as! NSDictionary))
            }
        }
    }

    func saveFavorite(artist : OArtist) {

        if artist.trackId == nil {return}

        var favorites = NSMutableDictionary()
        let defaults = UserDefaults.standard
        if let favoritesDefaults : NSMutableDictionary = defaults.object(forKey: "favorites") as! NSMutableDictionary? {
            favorites =  NSMutableDictionary(dictionary: favoritesDefaults)
        }

        favorites.setValue(artist.toDictionary(), forKey: artist.trackId!)
        
        defaults.set(favorites, forKey: "favorites")
        defaults.synchronize()
    }
}
