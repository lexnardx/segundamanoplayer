//
//  PlayerViewController.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit
import SDWebImage



class PlayerView: UIView, PlayerPresenterDelegate {

    static let sharedPlayer = PlayerView()

    var artistContent : OArtist?

    var visibleOriginY = UIUtils.heightProportion(proportion: 100) - 120
    private var imageContent : UIImageView?
    private var titleLbl : UILabel?
    private var subtitleLbl : UILabel?
    private var desciptionLbl : UILabel?
    private var playBtn : UIButton?
    private var playerPresenter = PlayerPresenter()


    private var playerHidden = true

    private init() {
        super.init(frame: CGRect(x: 0, y: UIUtils.heightProportion(proportion: 100), width: UIUtils.widthProportion(proportion: 100), height: UIUtils.heightProportion(proportion: 9)))
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {



        let size = self.frame.size

        playerPresenter.delegate = self

        let blurBackground = UIVisualEffectView()
        blurBackground.frame = self.bounds
        blurBackground.effect = UIBlurEffect(style: .regular)
        self.addSubview(blurBackground)

        imageContent = UIImageView(frame: CGRect(x: size.width * 0.05, y: 0, width: size.height * 0.75, height: size.height * 0.75))
        imageContent?.contentMode = .scaleAspectFit
        imageContent?.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        imageContent?.center = CGPoint(x: imageContent!.center.x, y: size.height/2)
        self.addSubview(imageContent!)

        titleLbl = UILabel(frame: CGRect(x: size.width * 0.25, y: 0, width: size.width * 0.6, height: size.height * 0.25))
        titleLbl?.titleAppearance()
        titleLbl?.center = CGPoint(x: titleLbl!.center.x, y: size.height * 0.25)
        self.addSubview(titleLbl!)

        subtitleLbl = UILabel(frame: CGRect(x: size.width * 0.25, y: 0, width: size.width * 0.6, height: size.height * 0.25))
        subtitleLbl?.subtitleAppearance()
        subtitleLbl?.center = CGPoint(x: subtitleLbl!.center.x, y: size.height * 0.5)
        self.addSubview(subtitleLbl!)

        desciptionLbl = UILabel(frame: CGRect(x: size.width * 0.25, y: 0, width: size.width * 0.6, height: size.height * 0.25))
        desciptionLbl?.desciptionAppearance()
        desciptionLbl?.center = CGPoint(x: desciptionLbl!.center.x, y: size.height * 0.75)
        self.addSubview(desciptionLbl!)

        /*playBtn = UIButton(frame: CGRect(x: size.width * 0.85, y: 0, width: size.height * 0.35, height: size.height * 0.35))
        playBtn?.center = CGPoint(x: playBtn!.center.x, y: size.height/2)
        playBtn?.setImage(UIImage(named: "icon-pause"), for: .normal)
        self.addSubview(playBtn!)*/

    }

    func playContent(content:OArtist) {
        artistContent = content
        playerPresenter.playArtistContent(artist: content)
        loadContent(content: content)
        showIfNeeded()
    }

    func loadContent(content : OArtist)  {

        imageContent?.sd_setImage(with: URL(string: content.artworkUrl!), placeholderImage: UIImage())
        titleLbl?.text = content.trackName
        subtitleLbl?.text = content.albumName
        desciptionLbl?.text = content.name
    }

    func showIfNeeded() {
        if playerHidden {
            animatePlayer(show: true)
        }
    }

    func animatePlayer(show : Bool) {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.4, options: .curveEaseInOut, animations: {
            print("Animate Frame \(UIUtils.changeY(frame: self.frame, y: show ? self.visibleOriginY : UIUtils.heightProportion(proportion: 100)))")
            self.frame = UIUtils.changeY(frame: self.frame, y: show ? self.visibleOriginY : UIUtils.heightProportion(proportion: 100))
        }) { (completed : Bool) in
            self.playerHidden = !show
            print(self.frame)
        }
    }

    //MARK: - PlayerPresenterDelegate

    func didStartPlaying(artistContent: OArtist) {

    }

    func didEndPlay(artistContent: OArtist) {

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
