//
//  PlayerPresenter.swift
//  Player
//
//  Created by leonardo Orihuela on 8/12/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit
import AVFoundation

protocol PlayerPresenterDelegate : AnyObject{
    func didStartPlaying(artistContent : OArtist)
    func didEndPlay(artistContent : OArtist)
}


class PlayerPresenter: NSObject {

    weak var delegate : PlayerPresenterDelegate?
    private var player : AVPlayer?



    func playArtistContent(artist : OArtist) {

        if artist.trackUrl == nil {
            return
        }

        let item = AVPlayerItem(url: URL(string: artist.trackUrl!)!)
        player = AVPlayer(playerItem: item)
        player?.rate = 1.0
        player?.play()
    }

}
