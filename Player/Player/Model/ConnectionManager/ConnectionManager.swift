//
//  ConnectionManager.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

class ConnectionManager: NSObject {

    let baseURLArtist = "https://itunes.apple.com/search?entiy=musicArtist"

    typealias CompletionHandler = (_ result : Any?, _ error : Error?) -> Void

    func startGET(url : String, completionHandler : @escaping CompletionHandler) {
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()

        manager.get(url, parameters: nil, progress: nil, success: { (task: URLSessionDataTask!, responseObject: Any!) in
            completionHandler(responseObject,nil)
        }, failure: { (task: URLSessionDataTask!, error: Error!) in
            completionHandler(nil,error)
        })
    }


}
