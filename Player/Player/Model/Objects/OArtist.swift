//
//  OArtist.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

class OArtist: NSObject {

    var identifier : String?
    var name : String?
    var artworkUrl : String?
    var trackName : String?
    var albumName : String?
    var trackUrl : String?
    var trackId : String?

    init(fromJson:NSDictionary) {
        super.init()

        if let identifier = fromJson.value(forKey: "artistId") as? Int {
            self.identifier = String(identifier)
        }
        if let name = fromJson.value(forKey: "artistName") as? String {
            self.name = name
        }
        if let artworkUrl = fromJson.value(forKey: "artworkUrl100") as? String {
            self.artworkUrl = artworkUrl
        }
        if let trackName = fromJson.value(forKey: "trackName") as? String {
            self.trackName = trackName
        }
        if let trackUrl = fromJson.value(forKey: "previewUrl") as? String {
            self.trackUrl = trackUrl
        }
        if let albumName = fromJson.value(forKey: "collectionName") as? String {
            self.albumName = albumName
        }
        if let trackId = fromJson.value(forKey: "trackId") as? Int {
            self.trackId = String(trackId)
        }
    }

    func toDictionary() -> NSDictionary {
        let dictionary = NSMutableDictionary()

        if let identifier = self.identifier {
            dictionary.setValue(Int(identifier), forKey: "artistId")
        }
        if let name = self.name{
            dictionary.setValue(name, forKey: "artistName")
        }
        if let artworkUrl = self.artworkUrl {
            dictionary.setValue(artworkUrl, forKey: "artworkUrl100")
        }
        if let trackName = self.trackName {
            dictionary.setValue(trackName, forKey: "trackName")
        }
        if let trackUrl = self.trackUrl{
            dictionary.setValue(trackUrl, forKey: "previewUrl")
        }
        if let albumName = self.albumName {
            dictionary.setValue(albumName, forKey: "collectionName")
        }
        if let trackId = self.trackId {
            dictionary.setValue(Int(trackId), forKey: "trackId")
        }

        return dictionary
    }
}
