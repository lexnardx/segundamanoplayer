//
//  UILabel+Custom.swift
//  Player
//
//  Created by leonardo Orihuela on 8/11/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {

    func titleAppearance() {
        self.font = UIFont.systemFont(ofSize: 19)
        self.textColor = .mainFontColor
    }
    func subtitleAppearance() {
        self.font = UIFont.systemFont(ofSize: 16)
        self.textColor = .darkGrayFontColor
    }
    func desciptionAppearance() {
        self.font = UIFont.systemFont(ofSize: 14)
        self.textColor = .grayFontColor
    }
}
