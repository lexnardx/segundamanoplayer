//
//  UIUtils.swift
//  Player
//
//  Created by leonardo Orihuela on 8/11/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import Foundation
import UIKit

struct UIUtils {

    static func currentDeviceFrame() -> CGRect {
        return UIScreen.main.bounds
    }

    static func changeY(frame : CGRect, y : CGFloat) -> CGRect {
        return CGRect(x: frame.origin.x, y: y, width: frame.size.width, height: frame.size.height)
    }
    static func changeX(frame : CGRect, x : CGFloat) -> CGRect {
        return CGRect(x: x, y: frame.origin.y, width: frame.size.width, height: frame.size.height)
    }

    static func widthProportion(proportion : CGFloat) -> CGFloat {
        return proportion > 0 ? currentDeviceFrame().size.width * (proportion/100) : 0
    }

    static func heightProportion(proportion : CGFloat) -> CGFloat {
        return proportion > 0 ? currentDeviceFrame().size.height * (proportion/100) : 0
    }

}
