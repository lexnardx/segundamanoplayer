//
//  JSONHelper.swift
//  Player
//
//  Created by leonardo Orihuela on 8/11/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import Foundation

struct JSONHelper {

    static func parseArtists(dictionary : NSDictionary) -> Array<OArtist> {

        if dictionary.allKeys.count == 0 {return Array<OArtist>()}

        let array = dictionary.value(forKey: "results") as! Array<NSDictionary>
        var artists = Array<OArtist>()

        for item in array {
            let artist = OArtist(fromJson: item)
            if artist.identifier != nil {
                artists.append(artist)
            }
        }

        return artists
    }
}
