//
//  UIColor+Custom.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {

    static let mainColor =  UIColor(red:1.00, green:0.28, blue:0.26, alpha:1.00)

    static let mainFontColor =  UIColor(red:0.12, green:0.13, blue:0.15, alpha:1.00)
    static let darkGrayFontColor =  UIColor(red:0.56, green:0.56, blue:0.56, alpha:1.00)
    static let grayFontColor =  UIColor(red:0.77, green:0.77, blue:0.77, alpha:1.00)

}
