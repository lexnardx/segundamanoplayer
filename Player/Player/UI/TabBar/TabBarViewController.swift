//
//  TabBarViewController.swift
//  Player
//
//  Created by leonardo Orihuela on 8/9/19.
//  Copyright © 2019 Leonardo Orihuela. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }


    func setupView() {

        self.tabBar.tintColor = .mainColor

        let searchController = SearchViewController(nibName: "SearchViewController", bundle: nil)
        searchController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)

        let favoritesController = FavoritesViewController(nibName: "FavoritesViewController", bundle: nil)
        favoritesController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)

        self.viewControllers = [UINavigationController(rootViewController: searchController),UINavigationController(rootViewController: favoritesController)]
        self.delegate = self

         tabBar.invalidateIntrinsicContentSize()

        PlayerView.sharedPlayer.layer.zPosition = 100
        self.view.addSubview(PlayerView.sharedPlayer)
    }

    override func viewDidAppear(_ animated: Bool) {
        PlayerView.sharedPlayer.visibleOriginY =  self.tabBar.frame.origin.y - PlayerView.sharedPlayer.frame.size.height
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
